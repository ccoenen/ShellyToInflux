# Shelly To Influx

Reads info about your shelly plug and posts it to InfluxDB.

![grafana dashboard with two shellyplug s added to it](documentation/grafana-example.png)


## Setup (Telegraf)

Use `shellyplug-status.conf` with Telegraf, just put it into your `/etc/telegraf/telegraf.d` directory and insert your shellyplug URLs in the `url` section. Make sure to keep the `/status/0` part at the end.

Telegraf will post the measurements under `shellies` which was chosen because this is the canonical name for the mqtt endpoints. You can change this in `shellyplug-status.conf`.


## Testing (Telegraf)

To check if this new part of your config works, you can run it individually like this:

* from a binary  
  `telegraf --test --config ./shellyplug-status.conf`
* from docker (make sure to insert the full path to shellyplug-status.conf!)  
  `docker run --rm --volume /path/to/shellyplug-status.conf:/test-config.conf telegraf telegraf --test --config /test-config.conf`
* from within docker-compose  
  `docker-compose exec telegraf telegraf --test --config /etc/telegraf/telegraf.d/shellyplug-status.conf`


## Setup (Grafana)

You may also use the `shellyplug-dashboard.json` with Grafana, just import it as a dashboard. While setting up this dashboard, you can set your local energy price in this dashboard's variables:

![grafana variables: kwhPrice](documentation/grafana-variables.png)


## Fields and their units

Sheshellyplug Documentation below for further info. We're keeping almost all of the Shellyplug names, I don't see a reason to change them even though there are inconsistencies.

* **timestamp**: (number) Unix timestamp of the last energy counter value - we use this as timestamp in telegraf, taken from `unixtime`. Not itself a field.
* **totalwh**: (number) Total energy consumed by the attached electrical appliance in Watt-hours
* **power**: (number) Current real AC power being drawn, in Watts
* **temperature**: (number) Device temperature in °celsius
* **ison**: (bool) Whether the channel is turned ON or OFF
* **rssi**: (int) Wifi signal strength in dBm
* **has_update**: (bool) If there is a firmware update available


### additional fields for Gen 2 devices

* **voltage**: (number) voltage in volts.
* **current**: (number) current in amperes.


### additional fields for older Gen 1 devices

* **total** (legacy): (number) Total energy consumed by the attached electrical appliance in Watt-minute, only Gen 1 devices report this.
* **overpower** (legacy): (number) Value in Watts, on which an overpower condition is detected (only Gen 1 devices report this)
* **overtemperature** (legacy): (bool) true if device has overheated
* **is_valid** (legacy): (bool) Whether power metering self-checks OK
* **connected** (legacy): (bool) If it is connected to the wifi (somewhat moot since we could never receive a "false" on this one.)


## Tags

* **mac**: MAC address of the device. Format `AABBCCDDEEFF`, no seperators
* **ssid**: SSID of the currently connected wifi
* **ip**: Current IP. Format `123.123.123.123`
* **firmware**: Currently running firmware, taken from `update.old_firmware` of the API, renamed for clarity (Shelly Gen 1 devices, only)


## References

* <https://shelly-api-docs.shelly.cloud/gen1/#shelly-plug-plugs-meter-0>
* <https://shelly-api-docs.shelly.cloud/gen1/#shelly-plug-plugs-status>
* <https://shelly-api-docs.shelly.cloud/gen1/#shelly-plug-plugs-relay-0>
* <https://shelly-api-docs.shelly.cloud/gen1/#settings>
* <https://github.com/influxdata/telegraf/blob/master/plugins/inputs/http/README.md>
* <https://github.com/influxdata/telegraf/blob/master/plugins/parsers/json_v2/README.md>
